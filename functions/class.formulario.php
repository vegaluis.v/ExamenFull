<?php
/**
 * Created by PhpStorm.
 * User: alk
 * Date: 21/06/18
 * Time: 00:01
 */

class formulario extends base
{
    private $nombre;
    private $apellido;
    private $sexo;
    private $estadoCivil;
    private $dni;
    private $telefonoFijo;
    private $telefonoMovil;
    private $Partido;
    private $calle;
    private $nro_calle;
    private $objetivo;
    private $nombreEmpresa = array();
    private $actividadEmpresa = array();
    private $puesto = array();
    private $nivelEmpresa = array();
    private $paisEmpresa = array();
    private $desdeEmpresa = array();
    private $hastaEmpresa = array();
    private $areaPuestoEmpresa = array();
    private $descripcionEmpresa = array();
    private $personasACargoEmpresa = array();
    private $personaDeReferenciaEmpresa = array();
    private $casaDeEstudios;
    private $nivelEstudio;
    private $especialidadEstudio;
    private $desdeEstudio;
    private $hastaEstudio;
    private $idioma;
    private $oral;
    private $escrito;


    public function save($post)
    {
        try {
            $this->desdeEstudio = $post[desdeEstudio];
            $this->hastaEstudio = $post[hastaEstudio];

            if ($this->validarString($post[nombre], 's')) {
                $this->nombre = $post[nombre];
            } else {
                throw new Exception('El nombre ingresado es incorrecto.');
            }

            if ($this->validarString($post[apellido], 's')) {
                $this->apellido = $post[apellido];
            } else {
                throw new Exception('El apellido ingresado es incorrecto.');
            }
            if ($this->validarString($post[sexo], 'i')) {
                $this->sexo = $post[sexo];
            } else {
                throw new Exception('El sexo ingresado es incorrecto.');
            }
            if ($this->validarString($post[estadoCivil], 's')) {
                $this->estadoCivil = $post[estadoCivil];
            } else {
                throw new Exception('El Estado Civil ingresado es incorrecto.');
            }
            if ($this->validarString($post[dni], 'i')) {
                $this->dni = $post[dni];
            } else {
                throw new Exception('El DNI ingresado es incorrecto.');
            }
            if ($this->validarString($post[telefonoFijo], 'i')) {
                $this->telefonoFijo = $post[telefonoFijo];
            } else {
                throw new Exception('El Telefono Fijo ingresado es incorrecto.');
            }
            if ($this->validarString($post[$this->telefonoMovil], 'i')) {
                $this->telefonoMovil = $post[telefonoMovil];
            } else {
                throw new Exception('El Telefono Movil ingresado es incorrecto.');
            }
            if ($this->validarString($post[Partido], 'i')) {
                $this->Partido = $post[Partido];
            } else {
                throw new Exception('El nombre Partido es incorrecto.');
            }
            if ($this->validarString($post[calle], 'i')) {
                if ($post[calle] == 0 && $post[otra_calle] != '') {
                    if ($this->validarString($post[otra_calle], 's')) {
                        $id_calle_nueva = $this->insert('calles', ['nombre' => $post[otra_calle]]);
                        $this->calle = $id_calle_nueva;
                    } else {
                        throw new Exception('La calle que intenta agregar es incorrecto.');
                    }
                } else {
                    throw new Exception('La calle que intenta agregar se encuentra vacia.');
                }

            } else {
                throw new Exception('El calle ingresado es incorrecto.');
            }
            if ($this->validarString($post[nro_calle], 'i')) {
                $this->nro_calle = $post[nro_calle];
            } else {
                throw new Exception('El Nro de Calle ingresado es incorrecto.');
            }
            if ($this->validarString($post[objetivo], 's')) {
                $this->objetivo = $post[objetivo];
            } else {
                throw new Exception('El Objetivo ingresado es incorrecto.');
            }
            if ($this->validarString($post[casaDeEstudios], 's')) {
                $this->casaDeEstudios = $post[casaDeEstudios];
            } else {
                throw new Exception('El Casa de Estudio ingresado es incorrecto.');
            }
            if ($this->validarString($post[nivelEstudio], 's')) {
                $this->nivelEstudio = $post[nivelEstudio];
            } else {
                throw new Exception('El Estudio ingresado es incorrecto.');
            }
            if ($this->validarString($post[especialidadEstudio], 's')) {
                $this->especialidadEstudio = $post[especialidadEstudio];
            } else {
                throw new Exception('El Especialidad de Estudio ingresado es incorrecto.');
            }
            if ($this->validarString($post[idioma], 's')) {
                $this->idioma = $post[idioma];
            } else {
                throw new Exception('El Idioma ingresado es incorrecto.');
            }
            if ($this->validarString($post[oral], 'i')) {
                $this->oral = $post[oral];
            } else {
                throw new Exception('El Nivel Oral ingresado es incorrecto.');
            }
            if ($this->validarString($post[escrito], 'i')) {
                $this->escrito = $post[escrito];
            } else {
                throw new Exception('El Nivel Escrito ingresado es incorrecto.');
            }
            for ($i = 0; $i < count($post[nombreEmpresa]); $i++) {
                $this->desdeEmpresa[$i] = $post[desdeEmpresa][$i];
                $this->hastaEmpresa[$i] = $post[hastaEmpresa][$i];

                if ($this->validarString($post[nombreEmpresa][$i], 'is', '. -')) {
                    $this->nombreEmpresa[$i] = $post[nombreEmpresa][$i];
                } else {
                    throw new Exception('El Nombre de La empresa ingresado es incorrecto.');
                }
                if ($this->validarString($post[actividadEmpresa][$i], 's')) {
                    $this->actividadEmpresa[$i] = $post[actividadEmpresa][$i];
                } else {
                    throw new Exception('La actividadEmpresa ingresado es incorrecto.');
                }
                if ($this->validarString($post[puesto][$i], 's')) {
                    $this->puesto[$i] = $post[puesto][$i];
                } else {
                    throw new Exception('El puesto ingresado es incorrecto.');
                }
                if ($this->validarString($post[nivelEmpresa][$i], 's')) {
                    $this->nivelEmpresa[$i] = $post[nivelEmpresa][$i];
                } else {
                    throw new Exception('El nivelEmpresa ingresado es incorrecto.');
                }
                if ($this->validarString($post[paisEmpresa][$i], 'i')) {
                    $this->paisEmpresa[$i] = $post[paisEmpresa][$i];
                } else {
                    throw new Exception('El Pais de Empresa ingresado es incorrecto.');
                }
                if ($this->validarString($post[areaPuestoEmpresa][$i], 's')) {
                    $this->areaPuestoEmpresa[$i] = $post[areaPuestoEmpresa][$i];
                } else {
                    throw new Exception('El areaPuestoEmpresa ingresado es incorrecto.');
                }
                if ($this->validarString($post[descripcionEmpresa][$i], 's')) {
                    $this->descripcionEmpresa[$i] = $post[descripcionEmpresa][$i];
                } else {
                    throw new Exception('El descripcionEmpresa ingresado es incorrecto.');
                }
                if ($this->validarString($post[personasACargoEmpresa][$i], 'i')) {
                    $this->personasACargoEmpresa[$i] = $post[personasACargoEmpresa][$i];
                } else {
                    throw new Exception('El personasACargoEmpresa ingresado es incorrecto.');
                }
                if ($this->validarString($post[personaDeReferenciaEmpresa][$i], 's')) {
                    $this->personaDeReferenciaEmpresa[$i] = $post[personaDeReferenciaEmpresa][$i];
                } else {
                    throw new Exception('El personaDeReferenciaEmpresa ingresado es incorrecto.');
                }
                if ($this->validarString($post[nombreEmpresa][$i], 'i')) {
                    $this->nombreEmpresa[$i] = $post[nombreEmpresa][$i];
                } else {
                    throw new Exception('El Nombre de La empresa ingresado es incorrecto.');
                }

            }

            $id_domicilios = $this->insert('domicilios', [
                "id_calle" => $this->calle,
                "numero" => $this->nro_calle,
                "id_partido" => $this->Partido
            ]);
            if (!$id_domicilios) {
                throw new Exception('El Domicilio ingresado es incorrecto.');
            }
            $id_datosPersonales = $this->insert('datosPersonales', [
                "nombre" => $this->nombre,
                "apellido" => $this->apellido,
                "idSexo" => $this->sexo,
                "idEstadoCivil" => $this->estadoCivil,
                "dni" => $this->dni,
                "telCelular" => $this->telefonoMovil,
                "telFijo" => $this->telefonoFijo,
                "idDireccion" => $id_domicilios
            ]);
            if (!$id_datosPersonales) {
                throw new Exception('Los datos personales ingresado son incorrecto.');
            }

            if (!$this->insert('estudios', [
                "idDatoPersonal" => $id_datosPersonales,
                "casaEstudio" => $this->casaDeEstudios,
                "nivel" => $this->nivelEstudio,
                "especialidad" => $this->especialidadEstudio,
                "desde" => $this->desdeEstudio,
                "hasta" => $this->hastaEstudio
            ])) {
                throw new Exception('El estudios ingresado es incorrecto.');
            }

            if (!$this->insert('idiomas', [
                "idDatoPersonal" => $id_datosPersonales,
                "idioma" => $this->idioma,
                "oral" => $this->oral,
                "escrito" => $this->escrito
            ])) {
                throw new Exception('El idioma ingresado es incorrecto.');
            }

            if (!$this->insert('objetivosLaborales', [
                "idDatoPersonal" => $id_datosPersonales,
                "objetivo" => $this->objetivo
            ])) {
                throw new Exception('El Objetivo ingresado es incorrecto.');
            }

            for ($i = 0; $i < count($this->nombreEmpresa); $i++) {
                if (!$this->insert('experienciasLaborales', [
                    "idDatoPersonal" => $id_datosPersonales[$i],
                    "empresa" => $this->nombreEmpresa[$i],
                    "actividadEmpresa" => $this->actividadEmpresa[$i],
                    "puesto" => $this->puesto[$i],
                    "nivel" => $this->nivelEmpres[$i],
                    "pais" => $this->paisEmpresa[$i],
                    "desde" => $this->desdeEmpresa[$i],
                    "hasta" => $this->hastaEmpresa[$i],
                    "areaPuesto" => $this->areaPuestoEmpresa[$i],
                    "descripcion" => $this->descripcionEmpresa[$i],
                    "aCargo" => $this->personasACargoEmpresa[$i],
                    "referencia" => $this->personaDeReferenciaEmpresa[$i]
                ])) {
                    throw new Exception('La experiencia laboral es incorrecta.');
                }
            }
            throw new Exception('Se cargo correctamente', 1);
        } catch (Exception $e) {
            return ['code' => $e->getCode(), 'msj' => $e->getMessage()];
        }
    }


}