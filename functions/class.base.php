<?php
/**
 * Created by PhpStorm.
 * User: alkcer
 * Date: 10/06/18
 * Time: 21:47
 */
$GLOBALS["debug"] = true;

class base
{
    private $host = "127.0.0.1";
    private $username = "root";
//    private $password = "rO0T-2016";
    private $password = "sistemas2016";
    private $database = "ExamenFull";
    private $dbc;
    private $nombreCampo = 'password';
//=================================
    private $nombre;
    private $apellido;
    private $sexo;
    private $estadoCivil;
    private $dni;
    private $telefonoFijo;
    private $telefonoMovil;
    private $Partido;
    private $calle;
    private $nro_calle;
    private $objetivo;
    private $nombreEmpresa = array();
    private $actividadEmpresa = array();
    private $puesto = array();
    private $nivelEmpresa = array();
    private $paisEmpresa = array();
    private $desdeEmpresa = array();
    private $hastaEmpresa = array();
    private $areaPuestoEmpresa = array();
    private $descripcionEmpresa = array();
    private $personasACargoEmpresa = array();
    private $personaDeReferenciaEmpresa = array();
    private $casaDeEstudios;
    private $nivelEstudio;
    private $especialidadEstudio;
    private $desdeEstudio;
    private $hastaEstudio;
    private $idioma;
    private $oral;
    private $escrito;

    public function __construct()
    {
        $this->dBconectar();
    }

    public function dBconectar()
    {
        $this->close();
        $this->dbc = new mysqli($this->host, $this->username, $this->password, $this->database);
        $this->dbc->set_charset("utf8");
        if ($this->dbc->connect_errno) {
            return printf("Falló la conexión: %s\n", $this->dbc->connect_error);
        } else {
            return $this->dbc;
        }
    }

    public function close()
    {
        return mysqli_close($this->dbc);
    }

    public function ejecutar($sql)
    {
        try {
            $resultado = $this->dBconectar()->query($sql);
            if (!$resultado) {
                if ($GLOBALS["debug"]) {
                    throw new Exception("La consulta ejecutar() " . $sql . " tiene errores.<br>Error: " . mysqli_connect_error($resultado) . "<br>Codigo: " . mysqli_connect_errno($resultado) . "<br>");
                } else {
                    throw new Exception("Error en la consulta.");
                }
            } else {
                return true;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function obtener($sql)
    {
        $resultado = $this->dBconectar()->query($sql);

        if (!$resultado) {
            if (!$GLOBALS["debug"]) {
                echo "La consulta obtener() " . $sql . " tiene errores.<br>Error: " . mysqli_connect_error($resultado) . "<br>Codigo: " . mysqli_connect_errno($resultado) . "<br>";
            } else {
                echo "Error en la consulta.";
            }
            exit();
        } else {

            unset($tabla);
            //Devuelve un array multidimensional
            $tabla = array();
            while ($fila = mysqli_fetch_array($resultado, MYSQLI_ASSOC)) {
                array_push($tabla, $fila);
            }
            if ($resultado->num_rows == 1) {
                return $tabla[0];
            } elseif ($resultado->num_rows > 1) {
                return $tabla;
            } else {
                return false;
            }
        }
    }

    public function insert($tabla, $POST)
    {
        $values = "";
        $into = "";
        foreach ($POST as $campo => $valor) {
            $valor = trim($valor);
            //si el $campo es password lo encripto
            if ($campo == $this->nombreCampo) {
                $valor = sha1($valor);
            }
            $values .= "'" . $valor . "',";
            $into .= "" . $campo . ",";
        }
        $values = substr($values, 0, -1);
        $into = substr($into, 0, -1);

        $sql = "INSERT INTO " . $tabla . "(" . $into . ") VALUES (" . $values . ")";
        $resultado = $this->ejecutar($sql);
        if ($resultado) {
            return $this->obtenerId($tabla, $POST);
        } else {
            return false;
        }
    }

    public function obtenerId($tabla, $POST)
    {
        $sql_id = "SELECT id FROM $tabla WHERE ";
        if (is_array($POST)) {
            foreach ($POST as $campo => $valor) {
                $valor = trim($valor);
                //si el $campo es password lo encripto
                if ($campo == $this->nombreCampo) {
                    $valor = sha1($valor);
                }
                $sql_id .= " $campo LIKE '$valor' and";
            }
            $sql_id = substr($sql_id, 0, -3);
            $sql_id .= ' order by id DESC limit 1';
            $resultado = $this->obtener($sql_id);
            if ($resultado) {
                return $resultado['id'];
            } else {
                return $resultado;
            }
        } else {
            return false;
        }
    }

    public function obtenerPorId($tabla, $id)
    {
        $sql_id = "SELECT * FROM $tabla WHERE id=$id limit 1";
        $resultado = $this->obtener($sql_id);
        return $resultado;
    }

    public function eliminarPorId($tabla, $id)
    {
        $sql = "DELETE FROM " . $tabla . " WHERE id=" . $id;
        $resultado = $this->ejecutar($sql);
        return $resultado;
    }

    public function validarString($string, $cod = 'is', $add = '')
    {
        /*
         * $cod= is => int + string
         * $cod= i => solo int 0 al 9
         * $cod= s => solo string a a la z min y masyus
         */

        $iniStr = '/^[';
        $finStr = ']+$/';
        switch ($cod) {
            case 'is':
                $exp_reg = "0-9a-zA-Z";
                break;
            case 'i':
                $exp_reg = "0-9";
                break;
            case 's':
                $exp_reg = "a-zA-Z";
                break;
            default:
                $exp_reg = "0-9a-zA-Z";
                break;
        }

        if ($add != '') {
            $exp_reg .= "$add";
        }
        $expRegStr = $iniStr . $exp_reg . $finStr;
        if (preg_match($expRegStr, $string)) {
            return true;
        } else {
            return false;
        }
    }

    public function save($post)
    {
        try {
            $this->desdeEstudio = $post[desdeEstudio] . ' 00:00:00';
            $this->hastaEstudio = $post[hastaEstudio] . ' 00:00:00';

            if ($this->validarString($post[nombre], 's')) {
                $this->nombre = $post[nombre];
            } else {
                throw new Exception('El nombre ingresado es incorrecto.');
            }

            if ($this->validarString($post[apellido], 's')) {
                $this->apellido = $post[apellido];
            } else {
                throw new Exception('El apellido ingresado es incorrecto.');
            }
            if ($this->validarString($post[sexo], 'i')) {
                $this->sexo = $post[sexo];
            } else {
                throw new Exception('El sexo ingresado es incorrecto.');
            }
            if ($this->validarString($post[estadoCivil], 'i')) {
                $this->estadoCivil = $post[estadoCivil];
            } else {
                throw new Exception('El Estado Civil ingresado es incorrecto.');
            }
            if ($this->validarString($post[dni], 'i')) {
                $this->dni = $post[dni];
            } else {
                throw new Exception('El DNI ingresado es incorrecto.');
            }
            if ($this->validarString($post[telefonoFijo], 'i')) {
                $this->telefonoFijo = $post[telefonoFijo];
            } else {
                throw new Exception('El Telefono Fijo ingresado es incorrecto.');
            }
            if ($this->validarString($post[telefonoMovil], 'i')) {
                $this->telefonoMovil = $post[telefonoMovil];
            } else {
                throw new Exception('El Telefono Movil ingresado es incorrecto.');
            }
            if ($this->validarString($post[Partido], 'i')) {
                $this->Partido = $post[Partido];
            } else {
                throw new Exception('El nombre Partido es incorrecto.');
            }
            if ($this->validarString($post[calle], 'i')) {
                if ($post[calle] == 0 && $post[otra_calle] != '') {
                    if ($this->validarString($post[otra_calle], 's')) {
                        $id_calle_nueva = $this->insert('calles', ['nombre' => $post[otra_calle]]);
                        $post[calle] = $id_calle_nueva;
                    } else {
                        throw new Exception('La calle que intenta agregar es incorrecto.');
                    }
                } elseif ($post[calle] == 0) {
                    throw new Exception('La calle que intenta agregar se encuentra vacia.');
                }
                $this->calle = $_POST[calle];

            } else {
                throw new Exception('El calle ingresado es incorrecto.');
            }
            if ($this->validarString($post[nro_calle], 'i')) {
                $this->nro_calle = $post[nro_calle];
            } else {
                throw new Exception('El Nro de Calle ingresado es incorrecto.');
            }
            if ($this->validarString($post[objetivo], 'is', ' ')) {
                $this->objetivo = $post[objetivo];
            } else {
                throw new Exception('El Objetivo ingresado es incorrecto.');
            }
            if ($this->validarString($post[casaDeEstudios], 'is', ' ')) {
                $this->casaDeEstudios = $post[casaDeEstudios];
            } else {
                throw new Exception('La Casa de Estudio ingresada es incorrecta.');
            }
            if ($this->validarString($post[nivelEstudio], 'is')) {
                $this->nivelEstudio = $post[nivelEstudio];
            } else {
                throw new Exception('El Estudio ingresado es incorrecto.');
            }
            if ($this->validarString($post[especialidadEstudio], 'is')) {
                $this->especialidadEstudio = $post[especialidadEstudio];
            } else {
                throw new Exception('El Especialidad de Estudio ingresado es incorrecto.');
            }
            if ($this->validarString($post[idioma], 's')) {
                $this->idioma = $post[idioma];
            } else {
                throw new Exception('El Idioma ingresado es incorrecto.');
            }
            if ($this->validarString($post[oral], 'i')) {
                $this->oral = $post[oral];
            } else {
                throw new Exception('El Nivel Oral ingresado es incorrecto.');
            }
            if ($this->validarString($post[escrito], 'i')) {
                $this->escrito = $post[escrito];
            } else {
                throw new Exception('El Nivel Escrito ingresado es incorrecto.');
            }

            if (isset($post[nombreEmpresa])) {

                $cargaEmpresa = true;
                for ($i = 0; $i < count($post[nombreEmpresa]); $i++) {
                    $this->desdeEmpresa[$i] = $post[desdeEmpresa][$i];
                    $this->hastaEmpresa[$i] = $post[hastaEmpresa][$i];

                    if ($this->validarString($post[nombreEmpresa][$i], 'is', '. -')) {
                        $this->nombreEmpresa[$i] = $post[nombreEmpresa][$i];
                    } else {
                        throw new Exception('El Nombre de La empresa ingresado es incorrecto.');
                    }
                    if ($this->validarString($post[actividadEmpresa][$i], 's', ' .')) {
                        $this->actividadEmpresa[$i] = $post[actividadEmpresa][$i];
                    } else {
                        throw new Exception('La actividadEmpresa ingresado es incorrecto.');
                    }
                    if ($this->validarString($post[puesto][$i], 's', ' .')) {
                        $this->puesto[$i] = $post[puesto][$i];
                    } else {
                        throw new Exception('El puesto ingresado es incorrecto.');
                    }
                    if ($this->validarString($post[nivelEmpresa][$i], 's', ' .')) {
                        $this->nivelEmpresa[$i] = $post[nivelEmpresa][$i];
                    } else {
                        throw new Exception('El nivelEmpresa ingresado es incorrecto.');
                    }
                    if ($this->validarString($post[paisEmpresa][$i], 's', ' .')) {
                        $this->paisEmpresa[$i] = $post[paisEmpresa][$i];
                    } else {
                        throw new Exception('El Pais de Empresa ingresado es incorrecto.');
                    }
                    if ($this->validarString($post[areaPuestoEmpresa][$i], 's', ' .')) {
                        $this->areaPuestoEmpresa[$i] = $post[areaPuestoEmpresa][$i];
                    } else {
                        throw new Exception('El areaPuestoEmpresa ingresado es incorrecto.');
                    }
                    if ($this->validarString($post[descripcionEmpresa][$i], 's', ' .')) {
                        $this->descripcionEmpresa[$i] = $post[descripcionEmpresa][$i];
                    } else {
                        throw new Exception('El descripcionEmpresa ingresado es incorrecto.');
                    }
                    if ($this->validarString($post[personasACargoEmpresa][$i], 'i')) {
                        $this->personasACargoEmpresa[$i] = $post[personasACargoEmpresa][$i];
                    } else {
                        throw new Exception('El personasACargoEmpresa ingresado es incorrecto.');
                    }
                    if ($this->validarString($post[personaDeReferenciaEmpresa][$i], 's', ' .-')) {
                        $this->personaDeReferenciaEmpresa[$i] = $post[personaDeReferenciaEmpresa][$i];
                    } else {
                        throw new Exception('El personaDeReferenciaEmpresa ingresado es incorrecto.');
                    }

                }
            } else {
                $cargaEmpresa = false;
            }
            $id_domicilios = $this->insert('domicilios', [
                "id_calle" => $this->calle,
                "numero" => $this->nro_calle,
                "id_partido" => $this->Partido
            ]);
            if (!$id_domicilios) {
                throw new Exception('El Domicilio ingresado es incorrecto.');
            }
            $id_datosPersonales = $this->insert('datosPersonales', [
                "nombre" => $this->nombre,
                "apellido" => $this->apellido,
                "idSexo" => $this->sexo,
                "idEstadoCivil" => $this->estadoCivil,
                "dni" => $this->dni,
                "telCelular" => $this->telefonoMovil,
                "telFijo" => $this->telefonoFijo,
                "idDireccion" => $id_domicilios
            ]);
            if (!$id_datosPersonales) {
                throw new Exception('Los datos personales ingresado son incorrecto.');
            }

            $estudios = $this->insert('estudios', [
                "idDatoPersonal" => $id_datosPersonales,
                "casaEstudio" => $this->casaDeEstudios,
                "nivel" => $this->nivelEstudio,
                "especialidad" => $this->especialidadEstudio,
                "desde" => $this->desdeEstudio,
                "hasta" => $this->hastaEstudio
            ]);
            if (!$estudios) {
                throw new Exception('El estudios ingresado es incorrecto.');
            }

            if (!$this->insert('idiomas', [
                "idDatoPersonal" => $id_datosPersonales,
                "idioma" => $this->idioma,
                "oral" => $this->oral,
                "escrito" => $this->escrito
            ])) {
                throw new Exception('El idioma ingresado es incorrecto.');
            }

            if (!$this->insert('objetivosLaborales', [
                "idDatoPersonal" => $id_datosPersonales,
                "objetivo" => $this->objetivo
            ])) {
                throw new Exception('El Objetivo ingresado es incorrecto.');
            }

//            throw new Exception($estudios);
            if ($cargaEmpresa) {
                for ($i = 0; $i < count($this->nombreEmpresa); $i++) {
                    if (!$this->insert('experienciasLaborales', [
                        "idDatoPersonal" => $id_datosPersonales[$i],
                        "empresa" => $this->nombreEmpresa[$i],
                        "actividadEmpresa" => $this->actividadEmpresa[$i],
                        "puesto" => $this->puesto[$i],
                        "nivel" => $this->nivelEmpres[$i],
                        "pais" => $this->paisEmpresa[$i],
                        "desde" => $this->desdeEmpresa[$i],
                        "hasta" => $this->hastaEmpresa[$i],
                        "areaPuesto" => $this->areaPuestoEmpresa[$i],
                        "descripcion" => $this->descripcionEmpresa[$i],
                        "aCargo" => $this->personasACargoEmpresa[$i],
                        "referencia" => $this->personaDeReferenciaEmpresa[$i]
                    ])) {
                        throw new Exception('La experiencia laboral es incorrecta.');
                    }
                }
            }
            throw new Exception('Se cargo correctamente', 1);
        } catch (Exception $e) {
            return ['code' => $e->getCode(), 'msj' => $e->getMessage()];
        }
    }

    public function load()
    {
        $sql = "
        SELECT
          dp.id idPersona,
          dp.nombre,
          dp.apellido,
          IF(dp.idSexo=1,'Femenino','Masculino') sexo,
          ec.nombre estCiv,
          dp.dni,
          dp.telCelular,
          dp.telFijo,
          c.nombre calle,
          d.numero altura,
          part.nombre partido,
          pro.nombre provincia,
          pai.nombre pais,
          est.casaEstudio,
          est.nivel,
          est.especialidad,
          est.desde,
          est.hasta,
          idio.idioma,
          idio.oral,
          idio.escrito,
          ol.objetivo,
          elab.empresa,
          elab.actividadEmpresa,
          elab.puesto,
          elab.nivel,
          elab.pais,
          elab.desde,
          elab.hasta,
          elab.areaPuesto,
          elab.descripcion,
          elab.aCargo,
          elab.referencia
        FROM
          datosPersonales dp
          INNER JOIN domicilios d on dp.idDireccion=d.id
          INNER JOIN calles c on d.id_calle= c.id
          INNER JOIN partidos part on d.id_partido=part.id
          INNER JOIN provincias pro on part.id_provincia=pro.id
          INNER JOIN paises pai on pro.id_pais=pai.id
          INNER JOIN estadosCiviles ec on dp.idEstadoCivil=ec.id
          INNER JOIN estudios est on dp.id=est.idDatoPersonal
          INNER JOIN idiomas idio on dp.id=idio.idDatoPersonal
          INNER JOIN objetivosLaborales ol on dp.id=ol.idDatoPersonal
          INNER JOIN experienciasLaborales elab on dp.id=elab.idDatoPersonal
        ";
        $respuesta_load = $this->obtener($sql);
        return json_encode($respuesta_load);
    }

    public function delete($id)
    {
        $sql = "
        SELECT
          dp.id idPersona,
          d.id idDom
        FROM
          datosPersonales dp
          INNER JOIN domicilios d on dp.idDireccion=d.id
          where dp.id=$id limit 1
        ";
        $respuesta_load = $this->obtener($sql);
        if ($respuesta_load && count($respuesta_load) > 1) {
            if (isset($respuesta_load[idPersona])) {
//                return json_encode($respuesta_load);
                $this->eliminarPorId('datosPersonales',$respuesta_load[idPersona]);
                $this->eliminarPorId('domicilios',$respuesta_load[idDom]);
            }
        }
    }
}
