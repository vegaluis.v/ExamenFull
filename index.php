<?php
require_once './functions/class.base.php';
try {
//    instanciamos la clase
    $base = new base();
//    obtenemos las calles
    $autocomplete_calles = $base->obtener("SELECT id,Replace(nombre , '\'', ' ')nombre FROM calles order by nombre");
    if ($autocomplete_calles) {
        $js_calles = json_encode($autocomplete_calles);
        foreach ($autocomplete_calles as $autocomplete_calles_sep) {
            $jsAutocompleteCalles[] = $autocomplete_calles_sep['nombre'];
        }
    } else {
        throw new Exception("Calles para autocompletar no encontradas.<br> Vuelva a cargar la pagina.");
    }
//    obtenemos los paises
    $obtener_paises = $base->obtener("SELECT id,Replace(nombre , '\'', ' ')nombre FROM paises order by nombre");
    if ($obtener_paises) {
        $js_paises = json_encode($obtener_paises);
    } else {
        throw new Exception("Paises para autocompletar no encontrados.<br> Vuelva a cargar la pagina.");
    }
    //    obtenemos los estados civiles
    $obtener_estadosCiviles = $base->obtener("SELECT id,Replace(nombre , '\'', ' ')nombre FROM estadosCiviles order by nombre");
    if ($obtener_estadosCiviles) {
        $js_estadosCiviles = json_encode($obtener_estadosCiviles);
    } else {
        throw new Exception("Paises para autocompletar no encontrados.<br> Vuelva a cargar la pagina.");
    }
} catch (Exception $e) {
    echo $e->getMessage();
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/material.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <!--<link rel="stylesheet" href="css/owl.carousel.css">-->
    <link rel="stylesheet" href="css/style.css">

</head>
<body>
<header class="header">
    <div class="supNav">
        <a class="navbar-brand imgHeader" href="#" target="_blank">
            <img src="img/bac-header.png" alt="">
        </a>
    </div>

</header>
<section id="home">
    <div class="mainSection">
        <div class="container">
            <div class="row backMainSection">
                <div class="navbar navbar-default navStyle">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse"
                                    data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="navbar-collapse collapse">
                            <nav>
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="navItem"><a href="#home">Home</a></li>
                                    <li class="navItem"><a href="#about">Expo Coder</a></li>
                                    <li class="navItem"><a href="#service">Suscribite</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div><!--- END CONTAINER -->
                </div>
                <!-- END NAVBAR -->
                <p class="mayor">Expo Coder</p>
                <p class="menor">¿Querés desarrollar apps, juegos, webs y más? <br>Inscribite</p>
                <div class="col-md-8 col-md-offset-2 buttonMain">
                    <a class="btn btn-default btn-home-bg" href="#">Ver más</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bannerSection pb-50 pt-50" id="about">
    <div class="banner">
        <div class="container backBanner">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12 textBanner paraBanner">
                    <h1>
                        Más de 2.000 participantes aprendieron programación en la última edición
                    </h1>
                    <p>
                        Por segundo año consecutivo, la Ciudad desarrolla un innovador evento para que cualquier
                        ciudadano pueda aprender un nuevo lenguaje , el de la programación, que les permitirá armar una
                        página web, aplicaciones y juegos.
                    </p>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 imgBanner"/>
            </div>
        </div>
    </div>
</section>
<section class="containerSlider sliderBack pb-30 pt-30" id="about">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!--<div id="owl-demo">-->

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div class="col-sm-3"><img src="img/02.jpg" alt="" width="70%"></div>
                <div class="col-sm-3 textSlider"><p class="title">Novedades</p>
                    <p class="paragraph">Enterate de lo último en nuestro seminarios y workshops.</p></div>
                <div class="col-sm-3"><img src="img/08.jpg" alt="" width="70%"></div>
                <div class="col-sm-3 textSlider"><p class="title">Conectate</p>
                    <p class="paragraph">Conoce las empresas líderes del mercado, te están buscando!</p></div>
            </div>
            <div class="item">
                <div class="col-sm-3"><img src="img/03.jpg" alt="" width="70%"></div>
                <div class="col-sm-3 textSlider"><p class="title">Responsabilidades</p>
                    <p class="paragraph">Buscar todas las soluciones posibles a un problema, analizarlas y determinar la
                        más efectiva y eficiente.</p></div>
                <div class="col-sm-3"><img src="img/04.jpg" alt="" width="70%"></div>
                <div class="col-sm-3 textSlider"><p class="title">Tu lugar</p>
                    <p class="paragraph">Tu posición de trabajo esta compuesta por todo lo que te hace bien.</p></div>
            </div>
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>
<section class="formSection pb-50 pt-50" id="service">
    <div class="formBackground">
        <div class="container">
            <div class="row">
                <h1>
                    Completa tus datos para participar
                </h1>
                <form class="styleForm" id="formulario" autocomplete="off">
                    <div class="fieldForm">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <p>Datos personales</p>
                                </div>

                                <div class="col-md-offset-10 col-sm-offset-10 col-xs-offset-10">
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4 marginInputForm">
                                    <input type="text" class="formStyle string" name="nombre" placeholder="Nombre"
                                           required>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 marginInputForm">
                                    <input type="text" class="formStyle string" name="apellido" placeholder="Apellido"
                                           required>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 marginInputForm"> Sexo:
                                    <label class="radio-inline"><input type="radio" value="1" name="sexo" checked>Femenino</label>
                                    <label class="radio-inline"><input type="radio" value="2"
                                                                       name="sexo">Masculino</label>
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4 marginInputForm">
                                    <select id="id_estadoCivil" data-live-search="true" name="estadoCivil"
                                            aria-describedby="inputGroupPrepend2" class="formStyle selectpicker"
                                            title="Estado Civil"> </select>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 marginInputForm">
                                    <input type="text" class="formStyle numerico" name="dni" placeholder="Dni" required>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 marginInputForm">
                                    <input type="text" class="formStyle numerico" name="telefonoFijo"
                                           placeholder="Telefono Fijo"
                                           aria-describedby="inputGroupPrepend2" required>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 marginInputForm">
                                    <input type="text" class="formStyle numerico" name="telefonoMovil"
                                           placeholder="Telefono Movil"
                                           aria-describedby="inputGroupPrepend2" required>
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4 marginInputForm">
                                    <select id="id_pais" data-live-search="true"
                                            aria-describedby="inputGroupPrepend2" class="formStyle selectpicker"
                                            title="Pais"> </select>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 marginInputForm">
                                    <select id="id_provincia" data-live-search="true"
                                            aria-describedby="inputGroupPrepend2" class="formStyle selectpicker"
                                            title="Provincia"> </select>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 marginInputForm">
                                    <select id="id_ciudad" data-live-search="true"
                                            aria-describedby="inputGroupPrepend2" class="formStyle selectpicker"
                                            title="Ciudad" name="Partido"> </select>
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4 marginInputForm">

                                    <div id="div_id_calle_sel">
                                        <select id="id_calle" data-live-search="true"
                                                aria-describedby="inputGroupPrepend2" class="formStyle selectpicker"
                                                title="Calle" name="calle"> </select>
                                    </div>
                                    <div id="div_id_calle_imp">
                                        <input type="text" class="formStyle string" name="otra_calle"
                                               placeholder="Ingrese NUEVA Calle"
                                               aria-describedby="inputGroupPrepend2" id="otra_calle" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 marginInputForm">
                                    <input type="text" class="formStyle numerico" name="nro_calle" placeholder="Altura"
                                           aria-describedby="inputGroupPrepend2" id="nro_calle">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fieldForm">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <p>Objetivo Laboral</p>
                                </div>
                                <div class="col-md-offset-10 col-sm-offset-10 col-xs-offset-10">
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6 marginInputForm">
                                    <textarea class="formStyle string" name="objetivo" rows="3"
                                              maxlength="200"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fieldForm">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-2 col-sm-3 col-xs-3">
                                    <p>Experiencia Laboral</p>
                                </div>
                                <div class="col-md-1 col-sm-3 col-xs-3 marginInputForm">
                                    <p>
                                        <a class="" id="addExpLaboral"><i class="glyphicon glyphicon-plus"
                                                                          aria-hidden="true"></i></a>
                                    </p>
                                </div>
                                <div class="col-md-9 col-sm-6 col-xs-6">
                                </div>
                            </div>
                        </div>
                        <div id="divExperiencia">
                            <div id="campoResopuesta"></div>
                        </div>
                    </div>
                    <div id="exp"></div>
                    <div class="fieldForm">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <p>Estudios</p>
                                </div>
                                <div class="col-md-offset-10 col-sm-offset-10 col-xs-offset-10">
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-3 marginInputForm">
                                    <input type="text" class="formStyle" name="casaDeEstudios"
                                           placeholder="Lugar de Estudio" required>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-3 marginInputForm">
                                    <input type="text" class="formStyle" name="nivelEstudio" placeholder="Nivel"
                                           required>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-3 marginInputForm">
                                    <input type="text" class="formStyle" name="especialidadEstudio"
                                           placeholder="Especialidad" required>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1 marginInputForm">
                                    <input type="text" class="formStyle date-picker" name="desdeEstudio"
                                           placeholder="Desde"
                                           required>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-1 marginInputForm">
                                    <input type="text" class="formStyle date-picker" name="hastaEstudio"
                                           placeholder="Hasta"
                                           required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fieldForm">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <p>Idiomas</p>
                                </div>
                                <div class="col-md-offset-10 col-sm-offset-10 col-xs-offset-10">
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4 marginInputForm">
                                    <input type="text" class="formStyle" name="idioma" placeholder="Idioma" required>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 marginInputForm">
                                    Nivel Oral:
                                    <label class="radio-inline"><input type="radio" value="1" name="oral"
                                                                       checked>1</label>
                                    <label class="radio-inline"><input type="radio" value="2" name="oral">2</label>
                                    <label class="radio-inline"><input type="radio" value="3" name="oral">3</label>
                                    <label class="radio-inline"><input type="radio" value="4" name="oral">4</label>
                                    <label class="radio-inline"><input type="radio" value="5" name="oral">5</label>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 marginInputForm">
                                    Nivel Escrito:
                                    <label class="radio-inline"><input type="radio" value="1" name="escrito"
                                                                       checked>1</label>
                                    <label class="radio-inline"><input type="radio" value="2" name="escrito">2</label>
                                    <label class="radio-inline"><input type="radio" value="3" name="escrito">3</label>
                                    <label class="radio-inline"><input type="radio" value="4" name="escrito">4</label>
                                    <label class="radio-inline"><input type="radio" value="5" name="escrito">5</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <div class="row">
                            <div class="submitButtonForm col-md-2 col-sm-2 col-xs-2 col-md-offset-10 col-sm-2 col-sm-offset-8 col-xs-offset-8">
                                <button class="btn btn-primary" type="submit" id="btn-enviar" name="boton"
                                       value="btnEnviar">Enviar
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
                <div id="enviado"></div>
            </div>
        </div>

    </div>

</section>

<div id="respuesta-form"></div>
<footer>
    <div class="footer footer-buenosAires">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12 ">
                    <a class="navbar-brand imgFooter" href="#" target="_blank">
                        <img src="img/bafooter.png" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12"> <!--acercaSitioFooter-->
                    <p>ACERCA DE ESTE SITIO</p>
                    <ul>
                        <div class="">
                            <li class="">
                                <a href="#" title="">Términos y condiciones</a></li>
                            <li class="">
                                <a href="#" title="">Política de privacidad</a>
                            </li>
                            <li class="">
                                <a href="#" title="">Sobre esta web</a>
                            </li>
                            <li class="">
                                <a href="#" title="">Mapa de sitio</a>
                            </li>
                        </div>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6 telefonosFooter">
                    <p>TELÉFONOS ÚTILES</p>
                    <ul>
                        <li>
                            <a href="tel:103">
                                <span class="glyphicon glyphicon-earphone"></span>
                                <div>103 | Emergencias</div>
                            </a>
                        </li>
                        <li>
                            <a href="tel:107">
                                <span class="glyphicon glyphicon-earphone"></span>
                                <div>107 | SAME</div>
                            </a>
                        </li>
                        <li>
                            <a href="tel:108">
                                <span class="glyphicon glyphicon-earphone"></span>
                                <div>108 | Línea Social</div>
                            </a>
                        </li>
                        <li>
                            <a href="tel:147">
                                <span class="glyphicon glyphicon-earphone"></span>
                                <div>147 | Atención Ciudadana</div>
                            </a>
                        </li>
                        <li>
                            <a href="tel:144">
                                <span class="glyphicon glyphicon-earphone"></span>
                                <div>144 | Violencia de Género</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <p class="hidden-xs hidden-sm">CONTACTO</p>
                    space
                    <hr class="hidden-lg hidden-md">
                    <ul class="social">
                        <li><a class="social-fb" href="#" target="_blank"><img src="img/social-fb.svg" alt=""></a></li>
                        <li><a class="social-tw" href="#" target="_blank"><img src="img/social-tw.svg" alt=""></a></li>
                        <li><a class="social-yt" href="#" target="_blank"><img src="img/social-yt.svg" alt=""></a></li>
                        <li><a class="social-in" href="#" target="_blank"><img src="img/social-in.png" alt=""></a></li>
                        <li><a class="social-rss" href="#" target="_blank"><img src="img/social-rss.svg" alt=""></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 legal">
                    <hr>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 legalText pull-right">
                            <p>
                                Los contenidos de Gobierno de la Ciudad Autónoma de Buenos Aires están licenciados bajo
                                Creative Commons Reconocimiento 2.5 Argentina License
                            </p>
                        </div>
                        <div class="logoOficialCiudad col-md-6 col-sm-6 col-xs-12 pull-left">
										<span>
											<a class="navbar-brand bac-footer" href="#" target="_blank"><img
                                                        src="img/bac-footer.png" alt=""></a>
										</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>
<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/material.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

<!--<script src="js/owl.carousel.js"></script>-->
<script>
    var nextinput = 0;

    $(document).ready(function () {
        $('#div_id_calle_imp').hide();
        $('#otra_calle').removeAttr("required");
        $('#otra_calle').prop('disabled', true);
        var js_paises = <?php echo $js_paises; ?>;
        $.each(js_paises, function (pos, valor) {
            $('#id_pais').append($("<option/>", {
                value: valor.id,
                text: valor.nombre
            }));
        });
        var js_estadosCiviles = <?php echo $js_estadosCiviles; ?>;
        $.each(js_estadosCiviles, function (pos, valor) {
            $('#id_estadoCivil').append($("<option/>", {
                value: valor.id,
                text: valor.nombre
            }));
        });
        $('.selectpicker').selectpicker('refresh');
        $(function () {
            $('.date-picker').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'yy-mm-dd',
                onClose: function (dateText, inst) {
                    $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                }
            });
        });

    });

    $('#addExpLaboral').click(function () {
        var campo = '<div id="removeInput' + nextinput + '"><div class="container">\n' +
            '                                <div class="row">\n' +
            '                                    <div class="col-md-4 col-sm-4 col-xs-4 marginInputForm">\n' +
            '                                        <input type="text" class="formStyle" name="nombreEmpresa[]"\n' +
            '                                               placeholder="Nombre de la empresa" required>\n' +
            '                                    </div>\n' +
            '                                    <div class="col-md-4 col-sm-4 col-xs-4 marginInputForm">\n' +
            '                                        <input type="text" class="formStyle" name="actividadEmpresa[]"\n' +
            '                                               placeholder="Actividad de la Empresa" required>\n' +
            '                                    </div>\n' +
            '                                    <div class="col-md-3 col-sm-3 col-xs-3 marginInputForm">\n' +
            '                                        <input type="text" class="formStyle" name="puesto[]" placeholder="Puesto"\n' +
            '                                               aria-describedby="inputGroupPrepend2" required>\n' +
            '                                    </div>\n' +
            '                                    <div class="col-md-1  col-sm-1 col-xs-1 marginInputForm">\n' +
            '                                        <a onclick="eliminarDiv(' + nextinput + ');"><i class="glyphicon glyphicon-trash"\n' +
            '                                                                     aria-hidden="true"></i></a>\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                            <div class="container">\n' +
            '                                <div class="row">\n' +
            '                                    <div class="col-md-3 col-sm-3 col-xs-3 marginInputForm">\n' +
            '                                        <input type="text" class="formStyle" name="nivelEmpresa[]" placeholder="Nivel"\n' +
            '                                               required>\n' +
            '                                    </div>\n' +
            '                                    <div class="col-md-3 col-sm-3 col-xs-3 marginInputForm">\n' +
            '                                        <input type="text" class="formStyle" name="paisEmpresa[]" placeholder="Pais"\n' +
            '                                               required>\n' +
            '                                    </div>\n' +
            '                                    <div class="col-md-3 col-sm-3 col-xs-3 marginInputForm">\n' +
            '                                        <input type="text" class="formStyle numerico" name="desdeEmpresa[]" placeholder="Desde (aaaa-mm-dd)"\n' +
            '                                               required onkeypress="return validarExRegulares(event,\'i\',\'/-\');">\n' +
            '                                    </div>\n' +
            '                                    <div class="col-md-3 col-sm-3 col-xs-3 marginInputForm">\n' +
            '                                        <input type="text" class="formStyle numerico" name="hastaEmpresa[]" placeholder="Hasta (aaaa-mm-dd)"\n' +
            '                                               required  onkeypress="return validarExRegulares(event,\'i\',\'/-\');">\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                            <div class="container">\n' +
            '                                <div class="row">\n' +
            '                                    <div class="col-md-3 col-sm-3 col-xs-3 marginInputForm">\n' +
            '                                        <input type="text" class="formStyle" name="areaPuestoEmpresa[]"\n' +
            '                                               placeholder="Area" required>\n' +
            '                                    </div>\n' +
            '                                    <div class="col-md-3 col-sm-3 col-xs-3 marginInputForm">\n' +
            '                                        <input type="text" class="formStyle" name="descripcionEmpresa[]"\n' +
            '                                               placeholder="Descripcion" required>\n' +
            '                                    </div>\n' +
            '                                    <div class="col-md-3 col-sm-3 col-xs-3 marginInputForm">\n' +
            '                                        <input type="text" class="formStyle" name="personasACargoEmpresa[]"\n' +
            '                                               placeholder="Personas a cargo" required>\n' +
            '                                    </div>\n' +
            '                                    <div class="col-md-3 col-sm-3 col-xs-3 marginInputForm">\n' +
            '                                        <input type="text" class="formStyle" name="personaDeReferenciaEmpresa[]"\n' +
            '                                               placeholder="Persona de Referencia" required>\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                            </div><hr/></div>';
        nextinput++;
        $("#campoResopuesta").append(campo);

    });

    $("#formulario").submit(function (event) {
        event.preventDefault();
        respuesta = $("#respuesta-form");
        $.ajax({
            url: './ajax.php',
            type: 'POST',
            dataType: 'json',
            data: $('#formulario').serialize(),
            beforeSend: function () {
                respuesta.html('<div class="alert alert-info"> Procesando ... </div>');
            }
        })
            .done(function (response) {
                        console.log(response);
                    if (response.code == 1) {
                        respuesta.html('<div class="alert alert-success">' + response.msj + '</div>');
                        // document.getElementById('btn-enviar').disabled = true;
                    } else {
                        respuesta.html('<div class="alert alert-danger"><strong> Error: </strong>' + response.msj + '</div>');
                        // document.getElementById('btn-enviar').disabled = false;
                    }
                }
            )
            .fail(function () {
                respuesta.html('<div class="alert alert-danger"><strong> Error: </strong> al procesar la carga</div>');
            });
    });

    $('.string').keypress(function (event) {
        return validarExRegulares(event, 's');
    });

    $('.numerico').keypress(function (event) {
        return validarExRegulares(event, 'i');
    });

    $('#id_pais').change(function () {
        $("#id_pais option:selected").each(function () {
            var id_pais = $(this).val();
            // event.preventDefault();
            $.ajax({
                url: './ajax.php',
                type: 'POST',
                dataType: 'json',
                data: {id_pais: id_pais},
            })
                .done(function (response) {
                    //vaciamos el select de localidades.
                    $('#id_provincia').empty();
                    //cargamos el select de localidades.
                    $.each(response, function (pos, valor) {
                        $('#id_provincia').append($("<option/>", {
                            value: valor.id,
                            text: valor.nombre
                        }));
                    });
                    //añado las opciones
                    $('#id_provincia').append($("<option/>", {
                        value: 0,
                        text: 'OTRA'
                    }));
                    $('.selectpicker').selectpicker('refresh');
                })
                .fail(function () {
                    console.log("error");
                });
        });
    });

    $('#id_provincia').change(function () {
        $("#id_provincia option:selected").each(function () {
            var id_provincia = $(this).val();
            // event.preventDefault();
            $.ajax({
                url: './ajax.php',
                type: 'POST',
                dataType: 'json',
                data: {id_provincia: id_provincia},
            })
                .done(function (response) {
                    //vaciamos el select de localidades.
                    $('#id_ciudad').empty();
                    //cargamos el select de localidades.
                    $.each(response, function (pos, valor) {
                        $('#id_ciudad').append($("<option/>", {
                            value: valor.id,
                            text: valor.nombre
                        }));
                    });
                    $('#id_ciudad').append($("<option/>", {
                        value: 0,
                        text: 'OTRA'
                    }));
                    //añado las opciones del json de calles
                    var js_calles =<?php echo $js_calles; ?>;
                    $.each(js_calles, function (pos, valor) {
                        $('#id_calle').append($("<option/>", {
                            value: valor.id,
                            text: valor.nombre
                        }));
                    });
                    //añado las opciones
                    $('#id_calle').append($("<option/>", {
                        value: 0,
                        text: 'OTRA'
                    }));
                    $('.selectpicker').selectpicker('refresh');
                })
                .fail(function () {
                    console.log("error");
                });
        });
    });

    $('#id_calle').change(function () {
        $("#id_calle option:selected").each(function () {
            var id_calle = $(this).val();
            //var arrayJS = <?php //echo json_encode($jsAutocompleteCalles); ?>//;

            // event.preventDefault();
            if (id_calle == 0) {
                $('#div_id_calle_sel').hide();
                $('#div_id_calle_imp').show();
                $('#otra_calle').removeAttr("disabled");
                $('#otra_calle').prop('required', true);
            } else {

                $('#div_id_calle_sel').show();
                $('#div_id_calle_imp').hide();
                $('#otra_calle').removeAttr("required");
                $('#otra_calle').prop('disabled', true);
            }
        });
    });

    function validarExRegulares(event, cod = 'is', add = '') {
        /*
         * cod= is => int + string
         * cod= i => solo int 0 al 9
         * cod= s => solo string "a" a la "z" min y masyus
         */
        var valorStr = event.which || event.keyCode;
        var inSt = '^[';
        var St = '';
        var fnSt = ']+$';
        switch (cod) {
            case 'is':
                St = '0-9a-zA-Z';
                break;
            case 'i':
                St = '0-9';
                break;
            case 's':
                St = 'a-zA-Z';
                break;
        }
        // console.log(valorStr);
        if (valorStr == 8 || valorStr == 37 || valorStr == 39 || valorStr == 32 || valorStr == 9) {
            return true
        } else {

            var patron = new RegExp(inSt + St + add + fnSt);
            return patron.test(String.fromCharCode(valorStr));
        }
    }

    function eliminarDiv(divId) {
        $('#removeInput' + divId).remove();
        // console.log(divId);
    }
</script>
