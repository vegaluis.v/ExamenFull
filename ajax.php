<?php
/**
 * Created by PhpStorm.
 * User: alkcer
 * Date: 13/06/18
 * Time: 21:19
 */

if (isset($_POST)) {
    require_once './functions/class.base.php';
    $base = new base();

    if (isset($_POST['nombre']) && isset($_POST['apellido'])) {
        $respuesta = $base->save($_POST);
        echo json_encode($respuesta);
    }

    //    obtenemos las provincias
    if (isset($_POST['id_pais']) and $_POST['id_pais'] != '' and is_numeric($_POST['id_pais'])) {
        $obtener_provincias = $base->obtener("SELECT id,Replace(nombre , '\'', ' ')nombre FROM provincias WHERE id_pais=$_POST[id_pais]  order by nombre");
        if ($obtener_provincias) {
            echo json_encode($obtener_provincias);
        } else {
            $obtener_provincias[] = ["id" => "0", "nombre" => "--"];
            echo json_encode($obtener_provincias);
        }
    }

    if (isset($_POST['id_provincia']) and $_POST['id_provincia'] != '' and is_numeric($_POST['id_provincia'])) {
        $sql_partidos = "SELECT id,Replace(nombre , '\'', ' ')nombre FROM partidos WHERE id_provincia= $_POST[id_provincia]  order by nombre";
        $obtener_partidos = $base->obtener($sql_partidos);
        if ($obtener_partidos) {
            echo json_encode($obtener_partidos);
        } else {
            $obtener_provincias[] = ["id" => "0", "nombre" => "--"];
            echo json_encode($obtener_provincias);
        }
    }//fin de provincia-partido
}
